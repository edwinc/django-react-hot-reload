from django.conf.urls import url
from . import views

from django.views.generic import TemplateView

urlpatterns = [
    url(r'^home$', views.index, name='index'),
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    ]