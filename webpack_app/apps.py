# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class WebpackAppConfig(AppConfig):
    name = 'webpack_app'
